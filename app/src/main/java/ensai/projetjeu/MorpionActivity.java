package ensai.projetjeu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.Multiplayer;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import static java.lang.Math.min;
import static java.lang.Math.random;

/**
 * Created by ensai on 30/05/17.
 */

public class MorpionActivity extends Activity {
    private Morpion monMorpion;
    private boolean ended =false;
    private int victoiresCroix=0;
    private int victoiresRond =0;

    private int monNumeroJoueur;
    private boolean canPlay = false;
    private boolean gameStarted = false;
    final static int RC_SIGN_IN = 9001;
    final static int RC_INVITATION_INBOX = 10001;
    // can be any number as long as it's unique
    final static int RC_SELECT_PLAYERS = 10000;

    private boolean mResolvingConnectionFailure = false;
    private boolean mAutoStartSignInFlow = true;
    private boolean mSignInClicked = false;
    public GoogleApiClient mGoogleApiClient;
    public Room room;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.morpion_activity);
        mGoogleApiClient = MyApplication.mGoogleApiClient;
        mGoogleApiClient.connect();


    }








    // Handle the result of the connection resolution
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {

        if (requestCode == RC_SELECT_PLAYERS) {
            if (resultCode != Activity.RESULT_OK) {
                // user canceled
                return;
            }

            // get the invitee list
            Bundle extras = intent.getExtras();
            final ArrayList<String> invitees =
                    intent.getStringArrayListExtra(Games.EXTRA_PLAYER_IDS);

            // get auto-match criteria
            Bundle autoMatchCriteria = null;
            int minAutoMatchPlayers =
                    intent.getIntExtra(Multiplayer.EXTRA_MIN_AUTOMATCH_PLAYERS, 0);
            int maxAutoMatchPlayers =
                    intent.getIntExtra(Multiplayer.EXTRA_MAX_AUTOMATCH_PLAYERS, 0);

            if (minAutoMatchPlayers > 0) {
                autoMatchCriteria = RoomConfig.createAutoMatchCriteria(
                        minAutoMatchPlayers, maxAutoMatchPlayers, 0);
            } else {
                autoMatchCriteria = null;
            }

            // create the room and specify a variant if appropriate
            RoomConfig.Builder roomConfigBuilder = makeBasicRoomConfigBuilder();
            roomConfigBuilder.addPlayersToInvite(invitees);
            if (autoMatchCriteria != null) {
                roomConfigBuilder.setAutoMatchCriteria(autoMatchCriteria);
            }
            RoomConfig roomConfig = roomConfigBuilder.build();

            Games.RealTimeMultiplayer.create(mGoogleApiClient, roomConfig);

            // prevent screen from sleeping during handshake
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            startGame();
        }

        if (requestCode == RC_INVITATION_INBOX){
            if(resultCode != Activity.RESULT_OK){
                return;

            }
            Bundle extras = intent.getExtras();
            Invitation invitation =
                    extras.getParcelable(Multiplayer.EXTRA_INVITATION);
            //accept the invitation

            RoomConfig roomConfig = makeBasicRoomConfigBuilder()
                    .setInvitationIdToAccept(invitation.getInvitationId())
                    .build();

            Games.RealTimeMultiplayer.join(mGoogleApiClient,roomConfig);

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            startGame();
        }
    }



    public void showInvit(View v){
        Intent intent = Games.Invitations.getInvitationInboxIntent(mGoogleApiClient);
        startActivityForResult(intent,RC_INVITATION_INBOX);
    }
    public void startQuickGame(View v) {
        // auto-match criteria to invite one random automatch opponent.
        // You can also specify more opponents (up to 3).
        Bundle am = RoomConfig.createAutoMatchCriteria(1, 1, 0);

        // build the room config:
        RoomConfig.Builder roomConfigBuilder = makeBasicRoomConfigBuilder();
        roomConfigBuilder.setAutoMatchCriteria(am);
        RoomConfig roomConfig = roomConfigBuilder.build();

        // create room:

        Games.RealTimeMultiplayer.create(mGoogleApiClient, roomConfig);

        // prevent screen from sleeping during handshake
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // go to game screen
        startGame();
    }


    public void startGame(){

        findViewById(R.id.show_invite_game_button).setVisibility(View.GONE);
        findViewById(R.id.invite_game_button).setVisibility(View.GONE);
        findViewById(R.id.quick_game_button).setVisibility(View.GONE);
        findViewById(R.id.newGame).setVisibility(View.VISIBLE);
        findViewById(R.id.returnMenu).setVisibility(View.VISIBLE);
        findViewById(R.id.ScoreCroix).setVisibility(View.VISIBLE);
        findViewById(R.id.ScoreRond).setVisibility(View.VISIBLE);
        findViewById(R.id.grille).setVisibility(View.VISIBLE);
    }


    public void sendMessage(String s){
        String messageStr = s;
        byte[] message = messageStr.getBytes(StandardCharsets.UTF_8);
        Log.e("Morpion","Envoie un message à " + room.getParticipants().size());

        for(Participant p : room.getParticipants()){
            if(p.getParticipantId() != myParticipantID()) {
                Log.w("Morpion",messageStr + p.getParticipantId());
                Games.RealTimeMultiplayer.sendReliableMessage(mGoogleApiClient, null, message, room.getRoomId()
                        , p.getParticipantId());
            }

        }
    }

    public String myParticipantID(){
        return room.getParticipantId(Games.Players.getCurrentPlayer(mGoogleApiClient).getPlayerId());
    }
    public void newGame(View v){
        if(gameStarted) {
            sendMessage("[Morpion][NEWGAME]");
        }

    }

    public void startNewGame(){
        initialiserGrille();
        setMorpion(new Morpion(this));
        this.setEnded(false);
        gameStarted = false;
        initialiserJeux();
    }
    public void returnMenu(View v){
        Intent intent = new Intent(this, SelectGameActivity.class);
        startActivity(intent);

    }

    public void initialiserJeux(){
        double random = random();
        int myNum = 0;
        if(random > 0.5){
            myNum = 1;
        }
        else{
            myNum = 2;
        }
        monNumeroJoueur = myNum;

        sendMessage("[Morpion][START]["+myNum+"]");
    }

    public void startMessage(int i){
        Log.i("Morpion","Fonction startMessage activer");
        if(i == monNumeroJoueur){
            initialiserJeux();
            Log.i("Morpion","Fonction startMessage doit relancer une init");

        }

        else{
            gameStarted = true;
            if(monNumeroJoueur == 1){
                Toast.makeText(getBaseContext(), "Tu es les croix commence!", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(getBaseContext(), "Tu es les ronds, à ton adversaire de jouer", Toast.LENGTH_SHORT).show();
            }
            Log.i("Morpion","Fonction startMessage à lancer la game");

        }

    }

    public void playMessage(int i){
        Log.i("Morpion","Fonction play message a jouer un coup");
        Log.e("Morpion",String.valueOf(i));
        monMorpion.jouer(i);
        actualiserImage(i);
    }






    // create a RoomConfigBuilder that's appropriate for your implementation
    private RoomConfig.Builder makeBasicRoomConfigBuilder() {
        ListenerMorpion listtmp = new ListenerMorpion(this);
        return RoomConfig.builder(listtmp)
                .setMessageReceivedListener(listtmp)
                .setRoomStatusUpdateListener(listtmp);
    }


    public void startInvite(View v){
        Intent intent = Games.RealTimeMultiplayer.getSelectOpponentsIntent(mGoogleApiClient,1,1);
        startActivityForResult(intent,RC_SELECT_PLAYERS);

    }

    public void afficherRond(ImageView image) {
        image.setImageResource(R.drawable.rond);
    }

    public void afficherCroix(ImageView image) {
        image.setImageResource(R.drawable.croix);
    }

    public void afficherVide(ImageView image) {
        image.setImageResource(R.drawable.blanc);
    }

    /* problème avec le switch ca fait tout le temps le denier...*/
    public void afficherContenu(int numeroCase, ImageView image) {
        Log.e("", " case contient " + Integer.toString(monMorpion.getNumCase_Contenu().get(numeroCase)));

        switch (monMorpion.getNumCase_Contenu().get(numeroCase)) {
            case 0:
                afficherVide(image);
                break;
            case 2:
                afficherRond(image);
                break;
            case 1:
                afficherCroix(image);
                break;

        }

    }
    public void setEnded(boolean ended){
        this.ended =ended;
    }

    public void initialiserGrille() {

        GridLayout grid = (GridLayout) this.findViewById(R.id.grille);
        grid.removeAllViews();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int displaySize = min(displayMetrics.widthPixels, displayMetrics.heightPixels);
        ViewGroup.LayoutParams imageLayoutParams = new GridLayout.LayoutParams();
        /*Toast toast = Toast.makeText(getBaseContext(), "la largeur de l'écran est " + displayMetrics.widthPixels + " pixels", Toast.LENGTH_SHORT);
        toast.show();*/
        imageLayoutParams.width = displaySize / 3;
        imageLayoutParams.height = displaySize / 3;
        ImageView image = new ImageView(this);
        for (int i = 0; i < 9; ++i) { // Génération des images.
            image = new ImageView(this);
            image.setLayoutParams(new ViewGroup.LayoutParams(imageLayoutParams));
            image.setImageResource(R.drawable.blanc);
            image.setId(i + 1);
            grid.addView(image);
        }
    }

    public void actualiserImage(int numeroCase) {
        ImageView image = (ImageView) this.findViewById(numeroCase);
        afficherContenu(numeroCase, image);


    }

    private View.OnTouchListener handleTouch = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int x = (int) event.getX();
            int y = (int) event.getY();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    Log.i("Morpion", "touched down");
                    break;
                case MotionEvent.ACTION_MOVE:
                    Log.i("Morpion", "moving: (" + x + ", " + y + ")");
                    break;
                case MotionEvent.ACTION_UP:
                    Log.i("Morpion", "touched up");
                    break;
            }
            /*
            Toast toast = Toast.makeText(getBaseContext(), "touched down : (" + x + ", " + y + ")", Toast.LENGTH_SHORT);
            toast.show();
            */

            int Ymax = displayMetrics.heightPixels;
            int Xmax = displayMetrics.widthPixels;
            int minima = min(Ymax, Xmax);
            if(gameStarted) {
                if (((Integer) monMorpion.getJoueurActif()).equals(monNumeroJoueur)) {
                    Log.e("Morpion",String.valueOf(monMorpion.getJoueurActif() == (monNumeroJoueur)));
                    if (x < minima & y < minima) {
                        int num_col = (int) x * 3 / minima;
                        int num_ligne = (int) (y * 3 / minima);
                        int numero_case = 1 + num_col + 3 * num_ligne;
                        monMorpion.jouer(numero_case);
                        actualiserImage(numero_case);
                        if (!ended) {
                            if (monMorpion.getIsEnded()) {
                                setEnded(true);
                                if (monMorpion.getVainqueur() == 2) {
                                    Toast victoire = Toast.makeText(getBaseContext(), "les ronds ont gagné", Toast.LENGTH_SHORT);
                                    TextView maVue = (TextView) findViewById(R.id.ScoreRond);
                                    victoiresRond++;
                                    maVue.setText("Score rond : " + victoiresRond);
                                    victoire.show();
                                } else {
                                    Toast victoire = Toast.makeText(getBaseContext(), "les croix ont gagné", Toast.LENGTH_SHORT);
                                    TextView maVue = (TextView) findViewById(R.id.ScoreCroix);
                                    victoiresCroix++;
                                    maVue.setText("Score croix : " + victoiresCroix);
                                    victoire.show();
                                }
                            }
                        }

                    }
                } else {
                    Toast.makeText(getBaseContext(), "Ce n'est pas à toi de jouer", Toast.LENGTH_SHORT).show();
                }
            }
            else{
                Toast.makeText(getApplicationContext(), "On attends ton adversaire", Toast.LENGTH_SHORT).show();
            }


            return true;
        }
    };


    public void setMorpion(Morpion morpion) {
        this.monMorpion = morpion;
    }

    public Morpion getMorpion() {
        return monMorpion;
    }

    public void morpionGame() {
        setMorpion(new Morpion(this));
        View maVue = findViewById(R.id.grille);
        maVue.setOnTouchListener(handleTouch);
        initialiserGrille();
        initialiserJeux();
    }





}
