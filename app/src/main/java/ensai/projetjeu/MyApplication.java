package ensai.projetjeu;

import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by ensai on 23/05/17.
 */

public class MyApplication {
    public static MainActivity mainActivity;
    public static GoogleApiClient mGoogleApiClient;
    public MyApplication(MainActivity activity){
        mainActivity = activity;
        mGoogleApiClient = activity.mGoogleApiClient;
    }
}
