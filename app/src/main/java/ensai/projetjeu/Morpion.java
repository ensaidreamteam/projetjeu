package ensai.projetjeu;

import android.util.Log;

import java.util.HashMap;

/**
 * Created by ensai on 23/05/17.
 */

public class Morpion {
    public HashMap<Integer, Integer> numCase_contenu;
    public int joueur_actif = 1;
    public MorpionActivity activity;
    public boolean isEnded = false;
    private int vainqueur=0;
    /* On choisit 0 pour le vide, 1 pour une croix et 2 pour un rond */
    public HashMap<Integer, Integer> getNumCase_Contenu() {
        return numCase_contenu;
    }

    public void setIsEnded(boolean b) {
        this.isEnded = b;
    }
    public void setVainqueur(int vainqueur){
        this.vainqueur=vainqueur;
    }
    public int getVainqueur(){
        return this.vainqueur;
    }
    public boolean getIsEnded() {
        return this.isEnded;
    }

    public boolean sumLigne(int numeroLigne,int joueur) {
        boolean resultat = true;
        int start = 3 * (numeroLigne-1);
        for (int i = 1; i < 4; i++) {
            int numCase = i + start;
            resultat = resultat & getNumCase_Contenu().get(numCase).equals(joueur);
        }
        return resultat;
    }

    public boolean sumColonne(int numeroColonne,int joueur) {
        boolean resultat = true;
        for (int i = 1; i < 4; i++) {
            int numCase = 3*(i-1) + numeroColonne;
            resultat = resultat & getNumCase_Contenu().get(numCase).equals(joueur);
        }
        return resultat;
    }

    public boolean sumDiagonale(int joueur) {

        return getNumCase_Contenu().get(1).equals(joueur)&getNumCase_Contenu().get(5).equals(joueur)&getNumCase_Contenu().get(9).equals(joueur);
    }

    public boolean sumAntiDiagonale(int joueur) {
        return  getNumCase_Contenu().get(3).equals(joueur)&getNumCase_Contenu().get(5).equals(joueur)&getNumCase_Contenu().get(7).equals(joueur);
    }

    public boolean aGagne(int joueur) {
        boolean res = false;
        for (int i = 1; i < 4; i++) {
            boolean b1 = sumLigne(i,joueur);
            boolean b2 = sumColonne(i,joueur)  ;
            res = res | b1 | b2;
        }
        boolean diag_gauche = sumDiagonale(joueur) ;
        res = res | diag_gauche;
        boolean diag_droite = sumAntiDiagonale(joueur) ;
        res = res | diag_droite;
        return res;
    }

    public Morpion(MorpionActivity morp) {
        this.numCase_contenu = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            numCase_contenu.put(i, 0);
        }
        this.activity = morp;
    }

    public int count_croix() {
        int resultat = 0;
        for (int i = 0; i < 10; i++) {
            if (numCase_contenu.get(i) == 1) {
                resultat++;
            }
        }
        return resultat;
    }

    public int count_rond() {
        int resultat = 0;
        for (int i = 0; i < 10; i++) {
            if (numCase_contenu.get(i) == 2) {
                resultat++;
            }
        }
        return resultat;
    }

    public void actualiser_joueur_actif() {
        if (count_rond() == count_croix()) {
            setJoueurActif(1);
        } else {
            setJoueurActif(2);
        }
    }
    public int getJoueurActif(){
        return this.joueur_actif;
    }

    public void setJoueurActif(int i) {
        this.joueur_actif = i;
    }

    public void jouer(int numeroCase) {

        if (numCase_contenu.get(numeroCase) == 0 && getIsEnded()==false) {
            numCase_contenu.put(numeroCase, this.joueur_actif);
            if (aGagne(this.joueur_actif)) {
                setIsEnded(true);
                setVainqueur(this.joueur_actif);
            }
            this.actualiser_joueur_actif();
            this.activity.sendMessage("[Morpion][PLAY]["+numeroCase+"]");

        } else {
            Log.e("Morpion","oh tu veux cocher un truc déja coché");
        }
    }
}