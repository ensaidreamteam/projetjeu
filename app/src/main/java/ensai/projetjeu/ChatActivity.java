package ensai.projetjeu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.Multiplayer;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by ensai on 23/05/17.
 */

public class ChatActivity extends Activity{
    public String idUtilisateur1 = "1";
    public String idUtilisateur2 = "2";
    public SQLiteDatabase db;
    public boolean isStarted = false;
    public GoogleApiClient mGoogleApiClient;
    public Room room;
    final static int RC_INVITATION_INBOX = 10001;
    // can be any number as long as it's unique
    final static int RC_SELECT_PLAYERS = 10000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);
        mGoogleApiClient = MyApplication.mGoogleApiClient;
        mGoogleApiClient.connect();
        recupererDb();

    }








    // Handle the result of the connection resolution
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {

        if (requestCode == RC_SELECT_PLAYERS) {
            if (resultCode != Activity.RESULT_OK) {
                // user canceled
                return;
            }

            // get the invitee list
            Bundle extras = intent.getExtras();
            final ArrayList<String> invitees =
                    intent.getStringArrayListExtra(Games.EXTRA_PLAYER_IDS);

            // get auto-match criteria
            Bundle autoMatchCriteria = null;
            int minAutoMatchPlayers =
                    intent.getIntExtra(Multiplayer.EXTRA_MIN_AUTOMATCH_PLAYERS, 0);
            int maxAutoMatchPlayers =
                    intent.getIntExtra(Multiplayer.EXTRA_MAX_AUTOMATCH_PLAYERS, 0);

            if (minAutoMatchPlayers > 0) {
                autoMatchCriteria = RoomConfig.createAutoMatchCriteria(
                        minAutoMatchPlayers, maxAutoMatchPlayers, 0);
            } else {
                autoMatchCriteria = null;
            }

            // create the room and specify a variant if appropriate
            RoomConfig.Builder roomConfigBuilder = makeBasicRoomConfigBuilder();
            roomConfigBuilder.addPlayersToInvite(invitees);
            if (autoMatchCriteria != null) {
                roomConfigBuilder.setAutoMatchCriteria(autoMatchCriteria);
            }
            RoomConfig roomConfig = roomConfigBuilder.build();

            Games.RealTimeMultiplayer.create(mGoogleApiClient, roomConfig);

            // prevent screen from sleeping during handshake
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            startGame();
        }

        if (requestCode == RC_INVITATION_INBOX){
            if(resultCode != Activity.RESULT_OK){
                return;

            }
            Bundle extras = intent.getExtras();
            Invitation invitation =
                    extras.getParcelable(Multiplayer.EXTRA_INVITATION);
            //accept the invitation

            RoomConfig roomConfig = makeBasicRoomConfigBuilder()
                    .setInvitationIdToAccept(invitation.getInvitationId())
                    .build();

            Games.RealTimeMultiplayer.join(mGoogleApiClient,roomConfig);

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            startGame();
        }
    }


    public void startInvite(View v){
        Intent intent = Games.RealTimeMultiplayer.getSelectOpponentsIntent(mGoogleApiClient,1,1);
        startActivityForResult(intent,RC_SELECT_PLAYERS);

    }
    public void showInvit(View v){
        Intent intent = Games.Invitations.getInvitationInboxIntent(mGoogleApiClient);
        startActivityForResult(intent,RC_INVITATION_INBOX);
    }
    public void startQuickGame(View v) {
        // auto-match criteria to invite one random automatch opponent.
        // You can also specify more opponents (up to 3).
        Bundle am = RoomConfig.createAutoMatchCriteria(1, 1, 0);

        // build the room config:
        RoomConfig.Builder roomConfigBuilder = makeBasicRoomConfigBuilder();
        roomConfigBuilder.setAutoMatchCriteria(am);
        RoomConfig roomConfig = roomConfigBuilder.build();

        // create room:

        Games.RealTimeMultiplayer.create(mGoogleApiClient, roomConfig);

        // prevent screen from sleeping during handshake
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // go to game screen
        startGame();
    }


    public void startGame(){

        findViewById(R.id.show_invite_game_button).setVisibility(View.GONE);
        findViewById(R.id.invite_game_button).setVisibility(View.GONE);
        findViewById(R.id.send).setVisibility(View.VISIBLE);
        findViewById(R.id.recupConvers).setVisibility(View.VISIBLE);
        findViewById(R.id.monScroll).setVisibility(View.VISIBLE);
        findViewById(R.id.message).setVisibility(View.VISIBLE);

    }
    public void envoie(String s){
        String messageStr = s;
        byte[] message = messageStr.getBytes(StandardCharsets.UTF_8);
        Log.e("LOG","Envoie un message à " + room.getParticipants().size());

        for(Participant p : room.getParticipants()){
            if(p.getParticipantId() != myParticipantID()) {
                Games.RealTimeMultiplayer.sendReliableMessage(MyApplication.mainActivity.mGoogleApiClient, null, message, room.getRoomId()
                        , p.getParticipantId());
            }

        }
    }

    public String myParticipantID(){
        return room.getParticipantId(Games.Players.getCurrentPlayer(mGoogleApiClient).getPlayerId());
    }

    private RoomConfig.Builder makeBasicRoomConfigBuilder() {
        ListenerChat listtmp = new ListenerChat(this);
        return RoomConfig.builder(listtmp)
                .setMessageReceivedListener(listtmp)
                .setRoomStatusUpdateListener(listtmp);
    }

    public void startChat(){
        isStarted = true;
        for(Participant p : room.getParticipants()){
            if(p.getParticipantId() != myParticipantID()) {

                idUtilisateur2 = p.getPlayer().getPlayerId();
            }
            else{
                idUtilisateur1 = p.getPlayer().getPlayerId();
            }

        }
        Toast.makeText(this,"Vous pouvez commencer à tchater",Toast.LENGTH_SHORT);
    }

    public void setDb(SQLiteDatabase db) {
        this.db = db;
    }

    public void recevoirMessage(String s) {
        LinearLayout messages = (LinearLayout) findViewById(R.id.Messagerie);
        ScrollView monScroll = (ScrollView) findViewById(R.id.monScroll);
        LinearLayout.LayoutParams mesParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        TextView texte = new TextView(this);
        texte.setText(s);
        messages.addView(texte);
        idUtilisateur1 = Games.Players.getCurrentPlayer(mGoogleApiClient).getPlayerId();
        ChatBddHelper.saveMessage(texte.getText().toString(), idUtilisateur2, idUtilisateur1, db);
        monScroll.smoothScrollTo(0, messages.getBottom());

    }

    public void recupererDb() {
        if (doesDatabaseExist(getBaseContext())) {
            //Toast toast = Toast.makeText(getBaseContext(), "Database already created", Toast.LENGTH_SHORT);
            //toast.show();
            SQLiteDatabase db = openOrCreateDatabase("chat", SQLiteDatabase.CREATE_IF_NECESSARY, null);
            this.setDb(db);

        } else {
            // Toast toast = Toast.makeText(getBaseContext(), "No data for now", Toast.LENGTH_SHORT);
            //toast.show();
            ChatBddHelper bdd = new ChatBddHelper(getBaseContext());
            SQLiteDatabase readableDataBase = bdd.getReadableDatabase();
            this.setDb(readableDataBase);
        }
    }


    private boolean doesDatabaseExist(Context context) {
        File dbFile = context.getDatabasePath("chat");
        return dbFile.exists();
    }


    public void sendMessage(View v) {
        if(isStarted) {
            LinearLayout messages = (LinearLayout) findViewById(R.id.Messagerie);
            ScrollView monScroll = (ScrollView) findViewById(R.id.monScroll);
            LinearLayout.LayoutParams mesParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            EditText maVueTexte = (EditText) findViewById(R.id.message);
            Editable message = maVueTexte.getText();
            TextView texte = new TextView(this);
            texte.setGravity(Gravity.RIGHT);
            texte.setText(message);
            messages.addView(texte);
            ChatBddHelper.saveMessage(texte.getText().toString(), idUtilisateur1, idUtilisateur2, db);
            monScroll.smoothScrollTo(0, messages.getBottom());
            envoie(message.toString());
        }
        else{
            Toast.makeText(this,"En attente de la personne à qui parler",Toast.LENGTH_SHORT).show();;
        }

    }

    public void recupererConversation(View v) {
        ScrollView monScroll = (ScrollView) findViewById(R.id.monScroll);
        LinearLayout messages = (LinearLayout) findViewById(R.id.Messagerie);
        messages.removeAllViews();


        HashMap<Integer, Message> messagesRecuperes = ChatBddHelper.recupererConversation(
                idUtilisateur1, idUtilisateur2, db);
        Set cles = messagesRecuperes.keySet();

        Iterator ite = cles.iterator();
        int taille=ChatBddHelper.tailleConversation(db,idUtilisateur1,idUtilisateur2);
        TextView texte = new TextView(this);

        while (ite.hasNext()) {
            Object cle = ite.next();
            Message message = messagesRecuperes.get(cle);

            if(message.getIsSend()){
                texte = new TextView(this);
                texte.setText(message.getMessage());
                texte.setGravity(Gravity.RIGHT);
                messages.addView(texte);
            }
            else{

                texte = new TextView(this);
                texte.setText(message.getMessage());
                texte.setGravity(Gravity.LEFT);
                messages.addView(texte);

            }

        }
        monScroll.smoothScrollTo(0, messages.getBottom());

    }








}
