package ensai.projetjeu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import static java.lang.Math.min;

/**
 * Created by ensai on 09/05/17.
 */

public class GameActivity extends AppCompatActivity {
    private Morpion monMorpion;
    private boolean ended =false;
    private int victoiresCroix=0;
    private int victoiresRond =0;
    public void afficherRond(ImageView image) {
        image.setImageResource(R.drawable.rond);
    }

    public void afficherCroix(ImageView image) {
        image.setImageResource(R.drawable.croix);
    }

    public void afficherVide(ImageView image) {
        image.setImageResource(R.drawable.blanc);
    }

    /* problème avec le switch ca fait tout le temps le denier...*/
    public void afficherContenu(int numeroCase, ImageView image) {
        Log.e("", " case contient " + Integer.toString(monMorpion.getNumCase_Contenu().get(numeroCase)));

        switch (monMorpion.getNumCase_Contenu().get(numeroCase)) {
            case 0:
                afficherVide(image);
                break;
            case -1:
                afficherRond(image);
                break;
            case 1:
                afficherCroix(image);
                break;

        }

    }
    public void setEnded(boolean ended){
        this.ended =ended;
    }

    public void initialiserGrille() {

        GridLayout grid = (GridLayout) this.findViewById(R.id.grille);
        grid.removeAllViews();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int displaySize = min(displayMetrics.widthPixels, displayMetrics.heightPixels);
        ViewGroup.LayoutParams imageLayoutParams = new GridLayout.LayoutParams();
        /*Toast toast = Toast.makeText(getBaseContext(), "la largeur de l'écran est " + displayMetrics.widthPixels + " pixels", Toast.LENGTH_SHORT);
        toast.show();*/
        imageLayoutParams.width = displaySize / 3;
        imageLayoutParams.height = displaySize / 3;
        ImageView image = new ImageView(this);
        for (int i = 0; i < 9; ++i) { // Génération des images.
            image = new ImageView(this);
            image.setLayoutParams(new ViewGroup.LayoutParams(imageLayoutParams));
            image.setImageResource(R.drawable.blanc);
            image.setId(i + 1);
            grid.addView(image);
        }
    }

    public void actualiserImage(int numeroCase) {
        ImageView image = (ImageView) this.findViewById(numeroCase);
        afficherContenu(numeroCase, image);


    }

    private View.OnTouchListener handleTouch = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int x = (int) event.getX();
            int y = (int) event.getY();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    Log.i("TAG", "touched down");
                    break;
                case MotionEvent.ACTION_MOVE:
                    Log.i("TAG", "moving: (" + x + ", " + y + ")");
                    break;
                case MotionEvent.ACTION_UP:
                    Log.i("TAG", "touched up");
                    break;
            }
            /*
            Toast toast = Toast.makeText(getBaseContext(), "touched down : (" + x + ", " + y + ")", Toast.LENGTH_SHORT);
            toast.show();
            */

            int Ymax = displayMetrics.heightPixels;
            int Xmax = displayMetrics.widthPixels;
            int minima = min(Ymax, Xmax);
            if (x < minima & y < minima) {
                int num_col = (int) x * 3 / minima;
                int num_ligne = (int) (y * 3 / minima);
                int numero_case = 1 + num_col + 3 * num_ligne;
                monMorpion.jouer(numero_case);
                actualiserImage(numero_case);
                if(!ended){
                if (monMorpion.getIsEnded()) {
                    setEnded(true);
                    if(monMorpion.getVainqueur()==-1){
                        Toast victoire = Toast.makeText(getBaseContext(), "les ronds ont gagné", Toast.LENGTH_SHORT);
                        TextView maVue=(TextView) findViewById(R.id.ScoreRond);
                        victoiresRond++;
                        maVue.setText("Score rond : "+ victoiresRond);
                        victoire.show();
                    }
                    else{
                        Toast victoire = Toast.makeText(getBaseContext(), "les croix ont gagné", Toast.LENGTH_SHORT);
                        TextView maVue=(TextView) findViewById(R.id.ScoreCroix);
                        victoiresCroix ++;
                        maVue.setText("Score croix : "+victoiresCroix);
                        victoire.show(); 
                    }
                }}
            }

            return true;
        }
    };
    public void newGame(View v){
        initialiserGrille();
        setMorpion(new Morpion());
        this.setEnded(false);

    }
    public void returnMenu(View v){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }

    public void setMorpion(Morpion morpion) {
        this.monMorpion = morpion;
    }

    public Morpion getMorpion() {
        return monMorpion;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMorpion(new Morpion());
        setContentView(R.layout.activity_game);
        View maVue = findViewById(R.id.grille);
        maVue.setOnTouchListener(handleTouch);
        initialiserGrille();

        /*setContentView(R.layout.activity_game);
        */

    }
}
