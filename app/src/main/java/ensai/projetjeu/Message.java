package ensai.projetjeu;

/**
 * Created by ensai on 30/05/17.
 */

public class Message {
    String message;
    boolean isSend; /* isSend vaut 1 si c'est un message envoyé et 0 sinon*/

    public Message(String message, boolean isSend) {
        this.message = message;
        this.isSend = isSend;
    }

    public boolean getIsSend() {
        return this.isSend;
    }

    public String getMessage() {
        return this.message;
    }
}
