package ensai.projetjeu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

/**
 * Created by ensai on 30/05/17.
 */

public class SelectGameActivity extends Activity{

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_game_activity);
        findViewById(R.id.morpion_button).setVisibility(View.VISIBLE);
        Log.e("Tag","Je me suis crée");
    }

    public void goToMorpion(View v){
        Intent intent = new Intent(this,MorpionActivity.class);
        startActivity(intent);
    }

    public void signOutClick(View v){
        finish();

    }
    public void goToChat(View V){
        Intent intent = new Intent(this,ChatActivity.class);
        startActivity(intent);
    }
}
