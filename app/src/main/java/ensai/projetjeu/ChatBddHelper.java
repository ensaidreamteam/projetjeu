package ensai.projetjeu;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * Created by cecile on 09/05/2017.
 */
public class ChatBddHelper extends SQLiteOpenHelper {
    public static final String DBNAME = "chat";
    public static final int DBVERSION = 1;
    private static final String TAG = "chat";

    public ChatBddHelper(Context context) {
        super(context, DBNAME, null, DBVERSION);
    }

    public SQLiteDatabase db;

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE message(idMessage INTEGER PRIMARY KEY AUTOINCREMENT, idExpediteur String, idDestinataire String, texte TEXT)");



    }

    public static HashMap<Integer,Message> recupererConversation(String idUtilisateur ,String idAutreUtilisateur, SQLiteDatabase db){
        HashMap<Integer,Message> conversation=recupererMessagesEnvoyes(idUtilisateur ,idAutreUtilisateur ,db);
        HashMap<Integer,Message> messagesRecus=recupererMessagesRecus(idUtilisateur ,idAutreUtilisateur, db);
        conversation.putAll(messagesRecus);
        return conversation;
    }
    public static HashMap<Integer,Message> recupererMessagesEnvoyes(String idUtilisateur, String idAutreUtilisateur,SQLiteDatabase db){
        HashMap<Integer,Message> monResultat= new HashMap<>();
       Cursor res= db.rawQuery("SELECT idMessage,texte FROM message WHERE idExpediteur='"+idUtilisateur+"' and idDestinataire='"+idAutreUtilisateur+"'",null);
        res.moveToFirst();
        while(!res.isAfterLast()){
            Message monMessage=new Message(res.getString(1),true);
            monResultat.put(res.getInt(0),monMessage);
            res.moveToNext();
        }

        res.moveToFirst();
        return(monResultat);

    }
    public static HashMap<Integer,Message> recupererMessagesRecus(String idUtilisateur, String idAutreUtilisateur,SQLiteDatabase db){
        HashMap<Integer,Message> monResultat= new HashMap<>();
        Cursor res= db.rawQuery("SELECT idMessage,texte FROM message WHERE idDestinataire='"+idUtilisateur+"' and idExpediteur='"+idAutreUtilisateur+"'",null);
        res.moveToFirst();
        while(!res.isAfterLast()){
            Message monMessage=new Message(res.getString(1),false);
            monResultat.put(res.getInt(0),monMessage);
            res.moveToNext();
        }
        return(monResultat);

    }
    public static int tailleConversation(SQLiteDatabase db,String idUtilisateur,String idAutreUtilisateur) /*il s'agit du nombre de messages envoyés par l'utilisateur  vers l' autre utilisateur */ {
        Cursor res = db.rawQuery("SELECT count(*) FROM message WHERE idExpediteur='"+idUtilisateur+"' and idDestinataire='"+idAutreUtilisateur+"'", null);
        res.moveToFirst();
        return res.getInt(0);
    }

    public static void saveMessage(String message,String idExpediteur,String idDestinataire,SQLiteDatabase db){
        SQLiteStatement stmt = db.compileStatement("INSERT INTO message(idExpediteur,idDestinataire,texte) VALUES(?, ?, ?)");
        stmt.bindString(1, idExpediteur);
        stmt.bindString(2, idDestinataire);
        stmt.bindString(3, message);
        stmt.executeInsert();
        stmt.close();

    }




    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }



}