package ensai.projetjeu;

import android.content.Intent;
import android.util.Log;
import android.view.WindowManager;

import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Created by ensai on 31/05/17.
 */

public class ListenerChat implements RoomUpdateListener,
        RoomStatusUpdateListener,
        RealTimeMessageReceivedListener {
    // are we already playing?
    public boolean mPlaying = false;

    // at least 2 players required for our game
    final static int MIN_PLAYERS = 2;
    public ChatActivity activite;

    public ListenerChat(ChatActivity activite){
        this.activite = activite;
    }
    @Override
    public void onRealTimeMessageReceived(RealTimeMessage realTimeMessage) {
        byte[] message = realTimeMessage.getMessageData();
        String messageStr = new String(message, StandardCharsets.UTF_8);
        int startType = 1;
        int endType = 0;
        int nbCroch = 0;
        int i = 0;
        Log.e("Morpion",messageStr);
        while(i < messageStr.length() & nbCroch <2){
            if(messageStr.charAt(i)==']'){
                if(nbCroch == 0){
                    startType = i+1;
                }
                else{
                    endType = i+1;
                }
                nbCroch ++;
            }
            i++;

        }

        activite.recevoirMessage(messageStr);
        Log.e("Morpion",messageStr);

    }


    @Override
    public void onRoomConnecting(Room room) {

    }

    @Override
    public void onRoomAutoMatching(Room room) {
        Log.e("Morpion","Le serveur cherche un autre joueur");
    }

    @Override
    public void onPeerInvitedToRoom(Room room, List<String> list) {

    }


    @Override
    public void onPeerJoined(Room room, List<String> list) {
        Log.e("Morpion","Connection de quelqu'un à la room Succeed");
    }

    @Override
    public void onConnectedToRoom(Room room) {
        Log.e("Morpion","Connection de quelqu'un à la room Succeed");
    }




    @Override
    public void onP2PConnected(String s) {

    }

    @Override
    public void onP2PDisconnected(String s) {

    }




    @Override
    public void onLeftRoom(int i, String s) {

    }



    @Override
    public void onRoomCreated(int statusCode, Room room) {
        if (statusCode != GamesStatusCodes.STATUS_OK) {
            // let screen go to sleep
            activite.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            Log.e("Morpion","Création de la room Fail");
            // show error message, return to main screen.
        }
        Log.e("Morpion","Création de la room Succed");
        activite.room = room;

    }

    @Override
    public void onJoinedRoom(int statusCode, Room room) {
        if (statusCode != GamesStatusCodes.STATUS_OK) {
            // let screen go to sleep
            activite.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            Log.e("Morpion","Connection à la room Fail");
            // show error message, return to main screen.
        }
        activite.room = room;
        Log.e("Morpion","Connection à la room Succeed");
    }

    @Override
    public void onRoomConnected(int statusCode, Room room) {
        if (statusCode != GamesStatusCodes.STATUS_OK) {
            // let screen go to sleep
            activite.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            Log.e("Morpion","Connection DE la room Fail");
            // show error message, return to main screen.
        }
        activite.room = room;
        Log.e("Morpion","Connection DE la room Succeed");
    }


    // returns whether there are enough players to start the game
    boolean shouldStartGame(Room room) {
        int connectedPlayers = 0;
        for (Participant p : room.getParticipants()) {
            if (p.isConnectedToRoom()) ++connectedPlayers;
        }
        Log.e("Morpion",String.valueOf(connectedPlayers));
        activite.room = room;
        return connectedPlayers >= MIN_PLAYERS;

    }

    // Returns whether the room is in a state where the game should be canceled.
    boolean shouldCancelGame(Room room) {
        int connectedPlayers = 0;
        for (Participant p : room.getParticipants()) {
            if (p.isConnectedToRoom()) ++connectedPlayers;
        }
        return connectedPlayers <= MIN_PLAYERS;
    }

    @Override
    public void onPeersConnected(Room room, List<String> peers) {
        if (mPlaying) {
            // add new player to an ongoing game
        } else if (shouldStartGame(room)) {
            activite.room = room;
            Log.i("Morpion","La game devrait démarer");
            activite.startChat();
        }
        activite.room = room;
        Log.e("Morpion","Un participants ou plus sont connecté à la room");
    }

    @Override
    public void onPeersDisconnected(Room room, List<String> peers) {
        if (mPlaying) {
            Games.RealTimeMultiplayer.leave(activite.mGoogleApiClient, null,room.getRoomId());
            activite.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            Intent intent = new Intent(activite, SelectGameActivity.class);
            activite.startActivity(intent);
        } else if (shouldCancelGame(room)) {
            // cancel the game
            Games.RealTimeMultiplayer.leave(activite.mGoogleApiClient, null,room.getRoomId());
            activite.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            Intent intent = new Intent(activite, SelectGameActivity.class);
            activite.startActivity(intent);
        }
    }

    @Override
    public void onPeerLeft(Room room, List<String> peers) {
        // peer left -- see if game should be canceled
        if (mPlaying && shouldCancelGame(room)) {

            Games.RealTimeMultiplayer.leave(activite.mGoogleApiClient, null, room.getRoomId());
            activite.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    @Override
    public void onPeerDeclined(Room room, List<String> peers) {
        // peer declined invitation -- see if game should be canceled
        if (!mPlaying && shouldCancelGame(room)) {
            Games.RealTimeMultiplayer.leave(activite.mGoogleApiClient, null, room.getRoomId());
            activite.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    @Override
    public void onDisconnectedFromRoom(Room room) {
        // leave the room
        Games.RealTimeMultiplayer.leave(activite.mGoogleApiClient, null, room.getRoomId());

        // clear the flag that keeps the screen on
        activite.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // show error message and return to main screen
    }



}
